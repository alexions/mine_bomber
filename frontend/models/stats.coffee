class @Stats
	@baseHp = 10
	@gold = 0

	@showPanel: (index) ->
		$('#index_' + index).show()

	
	@died: (index) ->
		$('#index_' + index  + ' .health').text('0%').css({'background-position': -100})

	@damage: (index, hp) ->
		perc = 100 * hp / @baseHp
		$('#index_' + index  + ' .health').text(perc + '%').css({'background-position': -(100-perc)})

	@gold: (index, gold) ->
		$('#index_' + index  + ' span').html('[' + gold + ']')

	@bomb: (index, tool) ->
		$('#index_' + index  + ' .armo-img').src = tool.icon
		$('#index_' + index  + ' .armo-count').html('[' + tool.count + ']')


