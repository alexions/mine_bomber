var extend = require('extend');
var math = require('math');
var Tail = require(rootdir + '/backend/models/Tail.js').Tail;

// Tool model
exports.Tool = function (type) {

	var toolType = exports.Types[type];

	return toolType;
};

exports.Types = {
	0: {
		id: 0,
		name: 'pick',
		count: 1,
		power: 1,
		range: function (position, map) {
			var pickPos = {fill: 0};
			for (pos in position.ceils) {
				if (position.ceils[pos].fill > pickPos.fill) {
					pickPos = position.ceils[pos];
				}
			};
			pickPos.koef = 1;

			switch (position.direction) {
				case 'up':
					pickPos.y--;
					break;
				case 'down':
					pickPos.y++;
					break;
				case 'left':
					pickPos.x--;
					break;
				case 'right':
					pickPos.x++;
					break;
			};

			return [pickPos];
		},
		active: function (user) {

			if (user.room == null) {
				return false;
			}
			
			var range = this.range(user.position, rooms[user.room].map);
			var ceils = [];

			var i = 0
			for (pos in range) {
				rooms[user.room].map[range[pos].y][range[pos].x] = rooms[user.room].map[range[pos].y][range[pos].x].damage(this.power * range[pos].koef);
				ceils[i] = {
					position: range[pos],
					tail: rooms[user.room].map[range[pos].y][range[pos].x],
				};
				i++;	
			}

			io.sockets.in(user.room).emit('drilling', {'user': this.id, 'room': user.room, ceils: ceils});
		},

		increment: function (tool) {
			this.power = this.power + tool.power;
		},

	},

	1: {
		id: 1,
		name: 'bomb',
		count: 100,
		fireSprite: 5,
		bombSprite: 7,
		power: 10,
		rangeX: 5,
		rangeY: 5,
		bombPosition: function(position) {
			var bombPos = {fill: 0};
			for (pos in position.ceils) {
				if (position.ceils[pos].fill > bombPos.fill) {
					bombPos.fill = position.ceils[pos].fill;
					bombPos = position.ceils[pos];
				}
			};

			return bombPos;
		},

		range: function (position, map) {
/*
	[[ 0, .5, .5, .5,  0],
	 [.5,  1,  1,  1, .5],
	 [.5,  1, 'x', 1, .5],
	 [.5,  1,  1,  1, .5],
	 [ 0, .5, .5, .5,  0]],
*/
			var bombPos = {fill: 0};
			for (pos in position.ceils) {
				if (position.ceils[pos].fill > bombPos.fill) {
					bombPos.fill = position.ceils[pos].fill;
					bombPos = position.ceils[pos];
				}
			};

			posFires = {}

			for (var x = -math.floor(this.rangeX/2); x <= math.floor(this.rangeX/2); x++) {
				for (var y = -math.floor(this.rangeY/2); y <= math.floor(this.rangeY/2); y++) {
					var koef = 0;
					if ((x == -math.floor(this.rangeX/2) || x == math.floor(this.rangeX/2)) && (y == -math.floor(this.rangeY/2) || y == math.floor(this.rangeY/2))) {
						koef = 0;
					} else if (x == -math.floor(this.rangeX/2) || x == math.floor(this.rangeX/2) || y == -math.floor(this.rangeY/2) || y == math.floor(this.rangeY/2)) {
						koef = .5;
					} else {
						koef = 1;
					}

					if (koef) {
						posFires[x + '_' + y] = {
							x: bombPos.x,
							y: bombPos.y,
							koef: bombPos,
						}
						posFires[x + '_' + y].x = posFires[x + '_' + y].x + x;
						posFires[x + '_' + y].y = posFires[x + '_' + y].y + y;
						posFires[x + '_' + y].koef = koef;
					}
				}
			}

			return posFires;
		},
		timer: 4000,

		active: function (user) {
			if (this.count == 0) {
				return false;
			}

			var range = this.range(user.position, rooms[user.room].map);
			var power = this.power;
			var fireSprite = this.fireSprite;
			this.count--;
			var bombPos = new Tail(this.bombSprite);
			bombPos.pos = this.bombPosition(user.position);
			bombPos.animname = 'bomb';

			io.sockets.in(user.room).emit('set_bomb', {'user': user.id, 'room': user.room, ceil: bombPos});
			io.sockets.in(user.room).emit('updateStat', user.getInfo());

			setTimeout(function () {
				if (typeof(rooms[user.room]) == 'undefined') {
					return false;
				}
				var mapSizeColumn = rooms[user.room].sizeColumn;
				var mapSizeRow = rooms[user.room].sizeRow;
				var ceils = {};
				var i = 0;
				for (pos in range) {
					var x = range[pos].x;
					var y = range[pos].y;
					if (y >= 0 && y < mapSizeRow) {
						if (x >= 0 && x < mapSizeColumn) {
							var koef = range[pos].koef;
							rooms[user.room].map[range[pos].y][range[pos].x] = rooms[user.room].map[range[pos].y][range[pos].x].damage(power * range[pos].koef);
							for (userId in rooms[user.room].users) {
								for (ceil in users[userId].position.ceils) {
									if (users[userId].position.ceils[ceil].x == range[pos].x && users[userId].position.ceils[ceil].y == range[pos].y) {
										users[userId].damage(power * range[pos].koef * users[userId].position.ceils[ceil].fill);
									}
								}
							}
							ceils[i] = {
								position: {x: x, y: y, koef: koef},
								tail: rooms[user.room].map[range[pos].y][range[pos].x],
								fire: new Tail(fireSprite),
							}
							i++;
						}
					}
				}

				io.sockets.in(user.room).emit('boom', {'user': this.id, 'room': user.room, ceils: ceils});
			}, this.timer);
		},

		increment: function (tool) {
			this.count = this.count + tool.count;
		},
	},

	2: {
		id: 2,
		name: 'corsair',
		count: 100,
		fireSprite: 5,
		bombSprite: 15,
		power: 3,
		rangeX: 3,
		rangeY: 3,
		bombPosition: function(position) {
			var bombPos = {fill: 0};
			for (pos in position.ceils) {
				if (position.ceils[pos].fill > bombPos.fill) {
					bombPos.fill = position.ceils[pos].fill;
					bombPos = position.ceils[pos];
				}
			};

			return bombPos;
		},

		range: function (position, map) {
/*
	[[.5,  1, .5],
	 [ 1, 'x', 1],
	 [.5,  1, .5]],
*/
			var bombPos = {fill: 0};
			for (pos in position.ceils) {
				if (position.ceils[pos].fill > bombPos.fill) {
					bombPos.fill = position.ceils[pos].fill;
					bombPos = position.ceils[pos];
				}
			};

			posFires = {}

			for (var x = -math.floor(this.rangeX/2); x <= math.floor(this.rangeX/2); x++) {
				for (var y = -math.floor(this.rangeY/2); y <= math.floor(this.rangeY/2); y++) {
					var koef = 0;
					if ((x == -math.floor(this.rangeX/2) || x == math.floor(this.rangeX/2)) && (y == -math.floor(this.rangeY/2) || y == math.floor(this.rangeY/2))) {
						koef = .5;
					} else {
						koef = 1;
					}

					if (koef) {
						posFires[x + '_' + y] = {
							x: bombPos.x,
							y: bombPos.y,
							koef: bombPos,
						}
						posFires[x + '_' + y].x = posFires[x + '_' + y].x + x;
						posFires[x + '_' + y].y = posFires[x + '_' + y].y + y;
						posFires[x + '_' + y].koef = koef;
					}
				}
			}

			return posFires;
		},
		timer: 1000,

		active: function (user) {
			if (this.count == 0) {
				return false;
			}

			var range = this.range(user.position, rooms[user.room].map);
			var power = this.power;
			var fireSprite = this.fireSprite;
			this.count--;
			var bombPos = new Tail(this.bombSprite);
			bombPos.pos = this.bombPosition(user.position);
			bombPos.animname = 'corsair';

			io.sockets.in(user.room).emit('set_bomb', {'user': this.id, 'room': user.room, ceil: bombPos});
			io.sockets.in(user.room).emit('updateStat', user.getInfo());

			setTimeout(function () {
				if (typeof(rooms[user.room]) == 'undefined' || typeof(rooms[user.room].sizeColumn) == 'undefined') {
					return false;
				}

				var mapSizeColumn = rooms[user.room].sizeColumn;
				var mapSizeRow = rooms[user.room].sizeRow;
				var ceils = {};
				var i = 0;
				for (pos in range) {
					var x = range[pos].x;
					var y = range[pos].y;
					if (y >= 0 && y < mapSizeRow) {
						if (x >= 0 && x < mapSizeColumn) {
							var koef = range[pos].koef;
							rooms[user.room].map[range[pos].y][range[pos].x] = rooms[user.room].map[range[pos].y][range[pos].x].damage(power * range[pos].koef);
							for (userId in rooms[user.room].users) {
								for (ceil in users[userId].position.ceils) {
									if (users[userId].position.ceils[ceil].x == range[pos].x && users[userId].position.ceils[ceil].y == range[pos].y) {
										users[userId].damage(power * range[pos].koef * users[userId].position.ceils[ceil].fill);
									}
								}
							}
							ceils[i] = {
								position: {x: x, y: y, koef: koef},
								tail: rooms[user.room].map[range[pos].y][range[pos].x],
								fire: new Tail(fireSprite),
							}
							i++;
						}
					}
				}

				io.sockets.in(user.room).emit('boom', {'user': this.id, 'room': user.room, ceils: ceils});
			}, this.timer);
		},

		increment: function (tool) {
			this.count = this.count + tool.count;
		},
	},

	3: {
		id: 3,
		name: 'tnt',
		count: 10,
		fireSprite: 5,
		bombSprite: 16,
		power: 30,
		rangeX: 9,
		rangeY: 9,
		bombPosition: function(position) {
			var bombPos = {fill: 0};
			for (pos in position.ceils) {
				if (position.ceils[pos].fill > bombPos.fill) {
					bombPos.fill = position.ceils[pos].fill;
					bombPos = position.ceils[pos];
				}
			};

			return bombPos;
		},

		range: function (position, map) {
/*
	[[ 0, .5, .5, .5, .5, .5,  0],
	 [.5,  1,  1,  1,  1,  1, .5],
	 [.5,  1,  1,  1,  1,  1, .5],
	 [.5,  1,  1,  1,  1,  1, .5],
	 [.5,  1,  1, 'x', 1,  1, .5],
	 [.5,  1,  1,  1,  1,  1, .5],
	 [.5,  1,  1,  1,  1,  1, .5],
	 [.5,  1,  1,  1,  1,  1, .5],
	 [ 0, .5, .5, .5, .5, .5,  0]],
*/
			var bombPos = {fill: 0};
			for (pos in position.ceils) {
				if (position.ceils[pos].fill > bombPos.fill) {
					bombPos.fill = position.ceils[pos].fill;
					bombPos = position.ceils[pos];
				}
			};

			posFires = {}

			for (var x = -math.floor(this.rangeX/2); x <= math.floor(this.rangeX/2); x++) {
				for (var y = -math.floor(this.rangeY/2); y <= math.floor(this.rangeY/2); y++) {
					var koef = 0;
					if ((x == -math.floor(this.rangeX/2) || x == math.floor(this.rangeX/2)) && (y == -math.floor(this.rangeY/2) || y == math.floor(this.rangeY/2))) {
						koef = 0;
					} else if (x == -math.floor(this.rangeX/2) || x == math.floor(this.rangeX/2) || y == -math.floor(this.rangeY/2) || y == math.floor(this.rangeY/2)) {
						koef = .5;
					} else {
						koef = 1;
					}

					if (koef) {
						posFires[x + '_' + y] = {
							x: bombPos.x,
							y: bombPos.y,
							koef: bombPos,
						}
						posFires[x + '_' + y].x = posFires[x + '_' + y].x + x;
						posFires[x + '_' + y].y = posFires[x + '_' + y].y + y;
						posFires[x + '_' + y].koef = koef;
					}
				}
			}

			return posFires;
		},
		timer: 6000,

		active: function (user) {
			if (this.count == 0) {
				return false;
			}

			var range = this.range(user.position, rooms[user.room].map);
			var power = this.power;
			var fireSprite = this.fireSprite;
			this.count--;
			var bombPos = new Tail(this.bombSprite);
			bombPos.pos = this.bombPosition(user.position);
			bombPos.animname = 'tnt';

			io.sockets.in(user.room).emit('set_bomb', {'user': this.id, 'room': user.room, ceil: bombPos});
			io.sockets.in(user.room).emit('updateStat', user.getInfo());

			setTimeout(function () {
				if (typeof(rooms[user.room]) == 'undefined' || typeof(rooms[user.room].sizeColumn) == 'undefined') {
					return false;
				}

				var mapSizeColumn = rooms[user.room].sizeColumn;
				var mapSizeRow = rooms[user.room].sizeRow;
				var ceils = {};
				var i = 0;
				for (pos in range) {
					var x = range[pos].x;
					var y = range[pos].y;
					if (y >= 0 && y < mapSizeRow) {
						if (x >= 0 && x < mapSizeColumn) {
							var koef = range[pos].koef;
							rooms[user.room].map[range[pos].y][range[pos].x] = rooms[user.room].map[range[pos].y][range[pos].x].damage(power * range[pos].koef);
							for (userId in rooms[user.room].users) {
								for (ceil in users[userId].position.ceils) {
									if (users[userId].position.ceils[ceil].x == range[pos].x && users[userId].position.ceils[ceil].y == range[pos].y) {
										users[userId].damage(power * range[pos].koef * users[userId].position.ceils[ceil].fill);
									}
								}
							}
							ceils[i] = {
								position: {x: x, y: y, koef: koef},
								tail: rooms[user.room].map[range[pos].y][range[pos].x],
								fire: new Tail(fireSprite),
							}
							i++;
						}
					}
				}

				io.sockets.in(user.room).emit('boom', {'user': this.id, 'room': user.room, ceils: ceils});
			}, this.timer);
		},

		increment: function (tool) {
			this.count = this.count + tool.count;
		},
	},
};