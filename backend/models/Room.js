var Tail = require(rootdir + '/backend/models/Tail.js').Tail;

// Room model
exports.Room = function (roomId, map) {

	var matrix = map.map;
	var sizeColumn = map.sizeColumn;
	var sizeRow = map.sizeRow;
	map = [];
	for (y = 0; y < matrix.length; y++) {
		map[y] = [];
		for (x = 0; x < matrix[y].length; x++) {
			map[y][x] = new Tail(matrix[y][x], {x: x, y: y});
		}
	}

	return {
		id: roomId,
		users: {},
		usersCount: 0,
		matrix: matrix,
		map: map,
		sizeColumn: sizeColumn,
		sizeRow: sizeRow,

		addUser: function (userId) {
			this.users[userId] = userId;
			this.usersCount++;
		},	

		removeUser: function (userId) {
			delete(this.users[userId]);
			this.usersCount--;
		},

		getInfo: function () {
			var info = {
				id: this.id,
				users: this.usersCount,
			};

			return info;
		},

		destroy: function () {
			if (this.usersCount) {
				for (userId in this.users) {
					users[userId].leaveRoom[this];	
				}
			}
			delete(rooms[this.id]);
		},

		syncMap: function () {
			var matrix = [];
			for (y = 0; y < map.length; y++) {
				matrix[y] = [];
				for (x = 0; x < map[y].length; x++) {
					matrix[y][x] = map[y][x].type;
				}
			}

			this.matrix = matrix;
		}
	}
}