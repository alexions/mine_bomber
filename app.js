express = require('express');
app = express();
server = require('http').createServer(app);
io = require('socket.io').listen(8080);
rootdir = __dirname;

io.set('log level', 1);
server.listen(80);
app.use('/', express.static(rootdir + '/'));

var User = require(rootdir + '/backend/models/User.js').User;
var Room = require(rootdir + '/backend/models/Room.js').Room;
var Tail = require(rootdir + '/backend/models/Tail.js').Tail;
var TailTypes = require(rootdir + '/backend/models/Tail.js').Types;
var Tool = require(rootdir + '/backend/models/Tool.js').Tool;
var ToolTypes = require(rootdir + '/backend/models/Tool.js').Types;
var Map = require(rootdir + '/backend/models/Map.js').Map;

users = {};
rooms = {};

// Навешиваем обработчик на подключение нового клиента
io.sockets.on('connection', function (socket) {
	var events = new Events(socket);

	socket.on('event', function (data, callback) {
		if (typeof data.event !== 'undefined' && typeof events[data.event] !== 'undefined') {
			events[data.event](data, callback);
		} else {
			callback('Неверный формат данных');
		}
	});

	events.connection();

	socket.on('disconnect', events.disconnect);
});

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

var Events = function (socket) {
	var socket = socket;
	var user = null;

	return {
		connection: function () {
			if (typeof users[socket.id] == 'undefined') {
				users[socket.id] = new User(socket.id);
			}
			user = users[socket.id];

			user.tools[0] = new Tool(0);
			user.tools[1] = new Tool(1);
			user.tools[2] = new Tool(2);
			user.tools[3] = new Tool(3);
		},

		disconnect: function () {
			if (user.room) {
				user.leaveRoom(rooms[user.room]);
			}
			delete(users[user.id]);
		},

		get_rooms: function (data, callback) {
			var listRooms = {};

			for (room in rooms) {
				listRooms[room] = rooms[room].getInfo();
			}

			if (typeof callback == 'function') {
				callback(listRooms);
			}
		},

		create_room: function (data, callback) {
			var roomId = (new Date).getTime();
			var map = new Map();
			rooms[roomId] = new Room(roomId, map);

			if (typeof callback == 'function') {
				callback(rooms[roomId].getInfo());
			}
		},

		destroy_room: function (data, callback) {
			if (typeof data.room !== 'undefined') {
				rooms[data.room].destroy();

				var result = (typeof rooms[data.room] == 'undefined');
			} else {
				var result = false;
			}

			if (typeof callback == 'function') {
				callback(result);
			}
		},

		join_room: function (data, callback) {
			if (typeof rooms[data.room] !== 'undefined') {
				user.joinRoom(rooms[data.room]);
			}

			if (typeof callback == 'function') {
				callback(user.getInfo());
			}
		},

		leave_room: function (data, callback) {
			var room = typeof data.room !== 'undefined' ? data.room : user.room;
			if (typeof rooms[room] !== 'undefined') {
				user.leaveRoom(rooms[room]);
			}

			if (typeof callback == 'function') {
				callback(user.room);
			}
		},

		get_map: function (data, callback) {
			rooms[user.room].syncMap();
			var matrix = rooms[user.room].matrix;

            if (typeof callback == 'function') {
				callback({map: matrix});
			}
        },

		move: function (data, callback) {
			if (user.room && data.position) {
				if (data.position.ceils.length) {
					minX = maxX = data.position.ceils[0].x;
					minY = maxY = data.position.ceils[0].y;
					for (i = 0; i < data.position.ceils.length; i++) {
						minX = data.position.ceils[i].x < minX ? data.position.ceils[i].x : minX;
						maxX = data.position.ceils[i].x > maxX ? data.position.ceils[i].x : maxX;
						minY = data.position.ceils[i].y < minY ? data.position.ceils[i].y : minY;
						maxY = data.position.ceils[i].y > maxY ? data.position.ceils[i].y : maxY;
					}

					var mapSizeColumn = rooms[user.room].sizeColumn;
					var mapSizeRow = rooms[user.room].sizeRow;
					var ceils = {};
					for (y = minY-2; y <= maxY+2; y++) {
						if (y < mapSizeRow) {
							ceils[y] = {};
							for (x = minX-2; x <= maxX+2; x++) {
								if (x < mapSizeColumn) {
									if (x < 0 || y < 0) {
										ceils[y][x] = false
									} else {
										ceils[y][x] = user.checkPosition({x: x, y: y}, rooms[user.room].map);
									}
								}
							}
						}
					}

                    var tail = rooms[user.room].map[data.position.ceils[0].y][data.position.ceils[0].x];

                    if (tail.gold) {
                        user.gold += tail.gold;
                        rooms[user.room].map[data.position.ceils[0].y][data.position.ceils[0].x] = new Tail(0, {y: data.position.ceils[0].y, x: data.position.ceils[0].x});

                        var ceil = [];
                        ceil[0] = {
                            position: data.position.ceils[0],
                            tail: rooms[user.room].map[data.position.ceils[0].y][data.position.ceils[0].x]
                        };
                        io.sockets.in(user.room).emit('updateStat', user.getInfo());
                        io.sockets.in(user.room).emit('takeGold', {'room': user.room, ceils: ceil});
                    }

					user.position = data.position;
					if (typeof callback == 'function') {
						callback(ceils)
					}

					socket.broadcast.to(user.room).emit('move', {id: user.id, position: user.position, ceils_access: ceils});
				}
			}
		},

		action: function (data, callback) {
			var _data = {};

			if (typeof data.tool !== 'undefined') {
				user.tools[data.tool].active(user);
			}

			if (typeof data.action !== 'undefined') {
				switch (data.action) {
					case 'nextTool':
						_data.toolId = user.changeTool();
						break;
				}
			}

			if (typeof callback == 'function') {
				callback(_data);
			}
		},

		get_room_users: function (data, callback) {
			if (user.room) {
				var usersInfo = {};

				for (userId in rooms[user.room].users) {
					usersInfo[userId] = users[userId].getInfo();
				}

				if (typeof data.all == 'undefined' || data.all == false) {
					delete(usersInfo[user.id]);
				}

				if (typeof callback == 'function') {
					callback(usersInfo);
				}
			}
		},

	}
}
