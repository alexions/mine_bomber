
mapArr = [
  ['pol.jpg', 'pol.jpg'],
  ['pol.jpg', 'pol.jpg'],
  ['pol.jpg', 'pol.jpg']
]

class @Map
  
  @stage = null
  @playerLayer = null
  @toolLayer = null
  @mainLayer = null

  @sprites = []

  constructor: (sprites) ->
    @blobElems = []
    @sprites = sprites
    @playerLayer = new Kinetic.Layer
    @toolLayer = new Kinetic.Layer
    @mainLayer = new Kinetic.Layer
    @stage = new Kinetic.Stage(
      container: 'container'
      width: 870
      height: 330
    )
    @grid_size = 30

  loadImages: (sources, callback) ->
    images = []
    loadedImages = 0
    numImages = 0
    self = this
    for mapRow, mapRowIds in sources
      images[mapRowIds]=[]
      for mapColl, mapCollIds in mapRow
        numImages++
        images[mapRowIds][mapCollIds] = new Image()
        images[mapRowIds][mapCollIds].onload = ->
          callback images, self  if ++loadedImages >= numImages  
        images[mapRowIds][mapCollIds].src = @sprites['map'][mapColl]

  loadMap: (source) ->
    @loadImages(source, @initStage)

  initStage: (images, self)->

    blob = []

    for imageRow, imageRowIds in images
      blob[imageRowIds]=[]
      for imageElem, imageElemIds in imageRow
        blob[imageRowIds][imageElemIds] = new Kinetic.Image(
          x: 30 * imageElemIds
          y: 30 * imageRowIds
          image: imageElem
          width: 30
          height: 30  
        )
        self.mainLayer.add blob[imageRowIds][imageElemIds]

    self.blobElems = blob
    self.stage.add self.mainLayer
    self.stage.add self.toolLayer
    self.stage.add self.playerLayer

  changeCells: (ceils)->
    self = this
    $.each ceils, ->
      self.animateSprite @position.x * self.grid_size, @position.y*self.grid_size, @fire.sprite, @fire.spriteFrames, @fire.timer, 'boom'

    $.each ceils, ->
      map.changeCell @position.x, @position.y, @tail.sprite, true

    @renderAll()
    

  animateSprite: (x, y, sprite, frames, timer, animname, image) ->
    self = this
    if image
      bomb_sprite = new Kinetic.Sprite(
        x: x
        y: y
        image: image
        animation: animname
        animations: animations.bomb,
        frameRate: frames
        index: 0
      )
      
      map.toolLayer.add bomb_sprite
      map.stage.add map.toolLayer

      bomb_sprite.start()

      setTimeout ->
        bomb_sprite.remove()
      , timer
    else
      map.changeCellAnim(sprite, (image) ->
        self.animateSprite(x, y, sprite, frames, timer, animname, image)
      )


  changeCell: (x, y, to, dont_render)->
    
    image = new Image()
    stage = @stage
    layer = @mainLayer
    tool_layer = @toolLayer
    player_layer = @playerLayer
    blob = []

    image.onload = ->
      blob = new Kinetic.Image(
        x: 30 * x
        y: 30 * y
        image: image
        width: 30
        height: 30
        )
      layer.add blob

      if !dont_render == true
        stage.add layer
        stage.add player_layer
        stage.add tool_layer

    image.src = to

  renderAll: () ->
    @stage.add @mainLayer
    @stage.add @playerLayer
    @stage.add @toolLayer
  
  removeElem: (x, y)->
    blobElems = @blobElems[x][y]
    blobElems.remove()

  moveIt: (x, y)->
    blobElems = @blobElems[x][y]


  changeCellAnim: (to, callback) ->
    image = new Image()

    image.onload = ->
      callback(image)

    image.src = to
