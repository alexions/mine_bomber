class @Unit
	constructor: (params) ->
		@id = 0
		@sprite = params.sprite || ''
		@toolId = params.toolId || 1
		@position = 
			x: 90
			y: 90
		@old_position = 
			x: 90
			y: 90
		@grid_size = 30
		@timer_check = 200
		@move_scale = 20
		@direction = '';
		@action = 'walk';
		@unit_id = params.object_id
		@keys_type = if params.keys_type then params.keys_type else null
		@socket = params.socket
		@character = null
		@access_ceils = []
		@current_ceils = []
		@events = 
			changeDirection: false

		self = this

		if params.start_position
			pos = @getAbsolutePosition params.start_position.ceils
			@position.x = pos.x
			@position.y = pos.y

			@old_position.x = pos.x
			@old_position.y = pos.y

		if @keys_type != null
			$(document).bind 'keydown', (e) =>
				@processKeys(e)

			setInterval ->
				self.stepsTimer self
			, @timer_check

	leaveGame: () ->
		@character.remove()
		@socket.emit 'event', 
			event: 'leave_room'

	createCharacter: (image, path) ->
		self = this

		if image
			if (!$.isEmptyObject @character)
				@character.remove()

			@character = new Kinetic.Sprite(
				x: @position.x
				y: @position.y
				image: image
				animation: 'down'
				animations: animations.bomber
				frameRate: 7
				index: 0
			)
			
			map.playerLayer.add @character
			map.stage.add map.playerLayer

			@character.start()
		else
			map.changeCellAnim(path || self.sprite, (image) ->
				self.createCharacter(image)
			)


	getIntPosition: () ->
		int_y = parseInt @position.y / @grid_size
		int_x = parseInt @position.x / @grid_size

		position = 
			x: int_x,
			y: int_y

	getAbsolutePosition: (ceils) ->
		if ceils.length == 1
			x = ceils[0].x * @grid_size
			y = ceils[0].y * @grid_size
		else
			if ceils[0].x < ceils[1].x
				x = ceils[0].x * @grid_size + @grid_size * ceils[0].fill
				y = ceils[0].y * @grid_size

			else
				x = ceils[0].x * @grid_size
				y = ceils[0].y * @grid_size + @grid_size * ceils[0].fill

		position = 
			x: x,
			y: y
				

	changeDirection: (direction) ->

		if @direction != direction
			@actions 'walk'
			@events.changeDirection = true

			pos = @getIntPosition()

			if (@direction == 'up' || @direction == 'down') && (direction == 'left' || direction == 'right')
				y_shift = @position.y - pos.y * @grid_size

				if y_shift > (@grid_size / 2)
					@position.y += @grid_size - y_shift
				else
					@position.y -= y_shift

			if (@direction == 'left' || @direction == 'right') && (direction == 'up' || direction == 'down')
				x_shift = @position.x - pos.x * @grid_size

				if x_shift > (@grid_size / 2)
					@position.x += @grid_size - x_shift
				else
					@position.x -= x_shift

			@direction = direction

	moveTo: (x, y, fast) ->
		self = this
		pos = @getIntPosition()

		y_shift = @position.y - pos.y * @grid_size
		x_shift = @position.x - pos.x * @grid_size

		if fast == false
			if @keys_type != null
				ceils = []

				if x_shift > 0
					if x_shift > @grid_size / 2
						ceils[0] = 
							x: pos.x
							y: pos.y
							fill: 0.4
						ceils[1] = 
							x: pos.x + 1
							y: pos.y
							fill: 0.6
					else
						ceils[0] = 
							x: pos.x
							y: pos.y
							fill: 0.6
						ceils[1] = 
							x: pos.x + 1
							y: pos.y
							fill: 0.4

				else if y_shift > 0
					if y_shift > @grid_size / 2
						ceils[0] = 
							x: pos.x
							y: pos.y
							fill: 0.4
						ceils[1] = 
							x: pos.x
							y: pos.y + 1
							fill: 0.6
					else
						ceils[0] = 
							x: pos.x
							y: pos.y
							fill: 0.6
						ceils[1] = 
							x: pos.x
							y: pos.y + 1
							fill: 0.4

				else
					ceils[0] = 
						x: pos.x
						y: pos.y
						fill: 1

				@current_ceils = ceils

				@socket.emit 'event', 
					event: 'move'
					position:
						ceils: ceils
						direction: @direction
						action: @action
				, (ceils) ->
					self.access_ceils = ceils


		if @character != null
			position = @position
			old_position = @old_position

			x_diff = @position.x - @old_position.x
			y_diff = @position.y - @old_position.y

			anim = new Kinetic.Animation (frame) -> 
					if frame.time >= self.timer_check
						#self.character.setX position.x
						#self.character.setY position.y

						anim.stop()
					else
						move_percent = ((100 * frame.time) / self.timer_check) / 100

						move_x = x_diff * move_percent
						move_y = y_diff * move_percent

						self.character.setX old_position.x + move_x
						self.character.setY old_position.y + move_y
				, map.playerLayer

			anim.start()				

	processKeys: (e) ->
		code = if e.keyCode then e.keyCode else e.which
		
		if code == 87
			@setBomb()

		if code == 81
			@changeTool() 

		switch @keys_type
			when 'normal' then sides = 
					38: 'up'
					39: 'right'
					40: 'down'
					37: 'left'
			when 'wsad' then sides = 
					87: 'up'
					68: 'right'
					83: 'down'
					65: 'left'


		if sides[code] then @changeDirection sides[code]

	actions: (action, send_request) ->
		if @action != action
			changed = true
		else
			changed = false

		if action == 'walk'
			if changed then @character.setAnimation @direction

		else if action == 'drills'
			if changed then @character.setAnimation 'drills_' + @direction

			if send_request
				@socket.emit 'event', 
					event: 'action'
					tool: 0 #Drill
				, (data) ->

		@action = action


	stepsTimer: (self) ->
		if @events.changeDirection
			@character.setAnimation @direction
			@events.changeDirection = false

		x = self.position.x
		y = self.position.y

		pos = @getIntPosition()
		#abs_pos = if @current_ceils.length >0 then @getAbsolutePosition(@current_ceils) else []
		y_shift = @position.y - pos.y * @grid_size
		x_shift = @position.x - pos.x * @grid_size

		x_count = y_count = 0
		x_length_shit = 0

		$.each @access_ceils, ->
			y_count++
			$.each @, ->
				x_count++

		x_length_shit = if x_count == 6 then 1 else 0
		y_length_shit = if y_count == 6 then 1 else 0
		
		switch self.direction
			when 'up' 
				if @access_ceils[pos.y - 1 - y_length_shit][pos.x]
					y -= self.move_scale
					@actions 'walk', true
				else
					if y_shift > 0
						y -= y_shift

					@actions 'drills', true

			when 'right' 
				if @access_ceils[pos.y][pos.x + 1 + x_length_shit]
					x += self.move_scale
					@actions 'walk', true
				else
					if x_shift > 0
						x -= x_shift

					@actions 'drills', true

			when 'down'
				if @access_ceils[pos.y + 1 + y_length_shit][pos.x]
					y += self.move_scale
					@actions 'walk', true
				else
					if y_shift > 0
						y += @grid_size - y_shift

					@actions 'drills', true

			when 'left' 
				if @access_ceils[pos.y][pos.x - 1 - x_length_shit]
					x -= self.move_scale
					@actions 'walk'
				else
					if x_shift > 0
						x -= x_shift

					@actions 'drills', true


		
		self.old_position.x = self.position.x
		self.old_position.y = self.position.y

		self.position.x = x
		self.position.y = y

		self.moveTo(x, y, false)

	setBomb: ->
		@socket.emit 'event',
			event: 'action'
			tool: @toolId
		, (data) ->
			true

	changeTool: ->
		self = this
		@socket.emit 'event', 
			event: 'action'
			action: 'nextTool'
		, (data) ->
			self.toolId = data.toolId







